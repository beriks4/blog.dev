<?php
include_once('../../Models/Role.php');

$desc = trim($_REQUEST['role']);
$level = trim($_REQUEST['level']);

if (!$desc OR (strlen($desc) < 3)) {
    echo json_encode(['status' => 'role']);
    die();
}
if (!$level OR ($level < 1 OR $level > 6)) {
    echo json_encode(['status' => 'level']);
    die();
}

$role = new Role();
$role->description = $desc;
$role->level = $level;
$role->save();
echo json_encode(['status' => 'ok']);
die();