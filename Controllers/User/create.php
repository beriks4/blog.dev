<?php
include_once('../../Models/User.php');

$name = trim($_REQUEST['name']);
$email = trim($_REQUEST['email']);
$password = trim($_REQUEST['pass']);
$password1 = trim($_REQUEST['pass1']);
$rights = trim($_REQUEST['rights']);


if (!$name OR (strlen($name) < 3)) {
    echo json_encode(['status' => 'name']);
    die();
}
if (!$email OR (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
    echo json_encode(['status' => 'email']);
    die();
}
if (!$password OR strlen($password) < 4) {
    echo json_encode(['status' => 'pass1']);
    die();
}
if ($password != $password1) {
    echo json_encode(['status' => 'pass2']);
    die();
}


$check = User::check('email', $email);
if ($check == true) :
    $temp = User::getByEmail($email);
    echo json_encode([
        'id' => $temp->id,
        'status' => 'found'
    ]);
    die();
else :
    $User = new User();
    $User->name = $name;
    $User->email = $email;
    $User->password = md5($password);
    $User->rights = $rights;
    $User->save();

    $temp = User::getByEmail($User->email);
    echo json_encode([
        'id' => $temp->id,
        'status' => 'notfound'
    ]);
endif;
die();