<?php
include_once('../Models/User.php');
include_once('../Models/Role.php');
session_start();

$email = $_REQUEST['email'];
$password = $_REQUEST['password'];

if (!$password AND !$email) {
    echo json_encode(['status' => 'err3']);
    die();
}
if (!$password AND (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
    echo json_encode(['status' => 'err4']);
    die();
}
if (!$email OR (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
    echo json_encode(['status' => 'err2']);
    die();
}
if (!$password) {
    echo json_encode(['status' => 'err1']);
    die();
}

$user = User::getByEmail($email);
$role = Role::getById($user->rights);
$desc = $role->description;
if (md5($password) == $user->password) {
    $_SESSION['name'] = $user->name;
    $_SESSION['id'] = $user->id;
    $_SESSION['desc'] = $desc;
    $_SESSION['time'] = time();

    echo json_encode(['status' => 'ok']);
    die();
} else {
    echo json_encode(['status' => 'err5']);
    die();
}
