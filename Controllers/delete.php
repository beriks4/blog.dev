<?php
include_once('../Models/User.php');
include_once('../Models/Post.php');
include_once('../Models/Role.php');
session_start();

$obj = trim($_REQUEST['obj']);
$id = trim($_REQUEST['id']);
$access = trim($_REQUEST['access']);
$rights = User::getById($_SESSION['id'])->getRole()->level;

if (!$_SESSION OR ($_SESSION['id'] != $access AND ($rights <= 3))) :
    header('Location: ../403.html');
    die();
endif;

if ($obj == 'post') {
    Post::delete($id);
    header("Location: ../Views/Post/index.php?target=$access");
}
if ($obj == 'user') {
    User::delete($id);
    header('Location: ../Views/User/index.php');
}
if ($obj == 'role') {
    Role::delete($id);
    header('Location: ../Views/Role/index.php');
}
