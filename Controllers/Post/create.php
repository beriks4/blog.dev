<!--Настоятельно рекомендуется обратить внимание на параметр-->
<!--accept-charset в форме из которой берутся переменные, он-->
<!--должен быть "UTF-8"-->
<meta charset="UTF-8">
<?php
include_once('../../Models/Post.php');

$name = trim($_REQUEST['name']);

$content = trim($_REQUEST['content']);


$id_u = trim($_REQUEST['id']);

$post = new Post();
$post->name = $name;
$post->content = $content;
$post->id_u = $id_u;
$post->save();
?>
<script>
    document.location.href = '/../Views/Post/index.php?target=<?= $id_u ?>';
</script>
