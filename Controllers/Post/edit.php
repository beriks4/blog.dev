<?php
include_once('../../Models/Post.php');

$id = trim($_REQUEST['post_id']);
$name = trim($_REQUEST['name']);
$content = trim($_REQUEST['content']);

$post = Post::getById($id);
$post->name = $name;
$post->content = $content;
$post->save();
?>
<script>
    document.location.href = '/../Views/Post/index.php?target=<?= $post->id_u ?>';
</script>
