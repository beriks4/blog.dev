<?php
$errorname = trim($_REQUEST['err']);
?>
<!DOCTYPE html>

<head>
    <meta charset="UTF-8" />
    <title><?= $errorname ?> error page</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/create.css" rel="stylesheet">
</head>

<body  >

<div class="container">
    <div class="row">
        <div class="well col-lg-8 col-lg-offset-2 text-center" id="center">
            <div class="logo">
                <h1>OPPS, <?= $errorname ?> Error  !</h1>
            </div>
            <p class="text-muted">There is something wrong here, Please try later or check <?= $errorname ?>. </p>
            <div class="col-lg-6  col-lg-offset-3">
                <div class="btn-group btn-group-justified">
                    <a href="/index.php" class="btn btn-success">На главную</a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
