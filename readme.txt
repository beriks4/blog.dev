﻿Bootstrap/                          //файлы фреймворка Bootstrap
Configs/db.sql                      //запрос на создание таблиц БД
Configs/params-local.php            //параметры для БД
Controllers/                        //файлы контролирующие действия
Models/                             //Модели Юзера, постов
Views/                              //интерфейс для взаимодействия с пользователем
css/                                //файлы стилей
js/                                 //файлы скриптов
index.php                           //главная
403.html, 404.html, error.php       //файлы ошибок

Пример файла params-local.php
<?php
return [
    'hostname' => 'localhost',
    'dbName' => 'Test',
    'username' => 'root',
    'password' => '',
];