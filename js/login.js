﻿jQuery(function ($) {
    $('#form').submit(function (e) {
        e.preventDefault();
        call();
    });
})
function call() {
    var data = $('#form').serialize();
    $.post('../Controllers/login.php', data, function (response) {
        var data = JSON.parse(response);
        function clear() {
            $('#errname').empty();
            $('#errpass').empty();
            $('#erremail').empty();
        }
        switch (data.status) {
            case 'ok':
                clear();
                $('#ok').empty();
                $('#ok').append('All good!');
                document.location.href = '../index.php';
                break;
            case 'err1':
                clear();
                $('#errpass').append('Поле не может быть пустым !');
                break;
            case 'err2':
                clear();
                $('#erremail').append('Введите корректный e-mail !');
                break;
            case 'err3':
                clear();
                $('#erremail').append('Поле не может быть пустым !');
                $('#errpass').append('Поле не может быть пустым !');
                break;
            case 'err4':
                clear();
                $('#errpass').append('Поле не может быть пустым !');
                $('#erremail').append('Введите корректный e-mail !');
                break;
            case 'err5':
                clear();
                $('#errpass').append('Неверный email/пароль!');
                break;
        }
    });
}