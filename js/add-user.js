﻿jQuery(function ($) {
    $('#form').submit(function (e) {
        e.preventDefault();
        call();
    });
})
function call() {
    var data = $('#form').serialize();
    $.post('/../Controllers/User/create.php', data, function (response) {
        var data = JSON.parse(response);
        function clear() {
            $('#errname').empty();
            $('#errpass').empty();
            $('#erremail').empty();
            $('#errpass1').empty();
        }
        switch (data.status) {
            case 'found':
                clear();
                $('#erremail').append('Введенный вами e-mail занят !');
                break;
            case 'notfound':
                alert('Успех! Вы создали нового пользователя! Вы будете перенаправлены на страницу с постами пользователя');
                document.location.href = '/../Views/User/index.php?target=' + data.id;
                break;
            case 'name':
                clear();
                $('#errname').append('Имя должно состоять как минимум из 3 символов !');
                break;
            case 'email':
                clear();
                $('#erremail').append('Введите корректный E-mail !');
                break;
            case 'pass1':
                clear();
                $('#errpass').append('Пароль должен состоять как минимум из 4 символов !');
                break;
            case 'pass2':
                clear();
                $('#errpass1').append('Пароли должны совпадать !');
                break;
        }
    });
}