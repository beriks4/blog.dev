jQuery(function ($) {
    $('#form').submit(function (e) {
        e.preventDefault();
        call();
    });
})
function call() {
    var data = $('#form').serialize();
    $.post('/../Controllers/Role/create.php', data, function (response) {
        var data = JSON.parse(response);
        function clear() {
            $('#errrole').empty();
            $('#errlevel').empty();
        }
        switch (data.status) {
            case 'role':
                clear();
                $('#errrole').append('Имя должно состоять как минимум из 3 символов !');
                break;
            case 'level':
                clear();
                $('#errlevel').append('Ведите число от 1 до 6 !');
                break;
            case 'ok':
                clear();
                $('#ok').append('All good!');
                document.location.href = '/../Views/Role/index.php';
                break;
        }
    });
}
