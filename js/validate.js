//
//	jQuery Validate example script
//
//	Prepared by David Cochran
//
//	Free for your use -- No warranties, no guarantees!
//

$(document).ready(function(){

		$('#form').validate({
	    rules: {
	      name: {
	        minlength: 3,
	        required: true
	      },
	      email: {
	        required: true,
	        email: true
	      }
	    },
			highlight: function(element) {
				$(element).closest('.control-group').removeClass('success').addClass('error');
			}
	  });

}); // end document.ready