CREATE TABLE `roles` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`description` TEXT NULL,
	`access_level` INT(11) NULL DEFAULT '0',
	PRIMARY KEY (`id`)
);
CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` TEXT NULL,
	`email` VARCHAR(100) NULL DEFAULT NULL,
	`password` TEXT NULL,
	`role_id` INT(11) NULL DEFAULT '2',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `email` (`email`),
	INDEX `FK_users_roles` (`role_id`),
	CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
);
CREATE TABLE `posts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(35) NOT NULL,
	`date` DATE NOT NULL,
	`content` VARCHAR(500) NULL DEFAULT NULL,
	`id_u` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_posts_users` (`id_u`),
	CONSTRAINT `FK_posts_users` FOREIGN KEY (`id_u`) REFERENCES `users` (`id`)
);
INSERT INTO roles (description, access_level) VALUES ("Администратор", "6");
INSERT INTO roles (description, access_level) VALUES ("Модератор", "3");
INSERT INTO roles (description, access_level) VALUES ("Пользователь", "2");
INSERT INTO users (name, email, password, role_id) VALUES ("admin", "admin@gmail.com", "21232f297a57a5a743894a0e4a801fc3", 1);