<?php

class Connect
{
    private $hostname;
    private $dbName;
    private $username;
    private $password;

    public function connect()
    {
        if (file_exists(__DIR__ . '/../Configs/params-local.php')) :
            $conf = include(__DIR__ . '/../Configs/params-local.php');
         else : ?>
            <script>
                document.location.href = '../error.php';
            </script>
        <?php endif;
        $this->hostname = $conf['hostname'];
        $this->username = $conf['username'];
        $this->password = $conf['password'];
        $this->dbName = $conf['dbName'];

        $mysqli = mysqli_init();
//      Connect to DB
        if (!$mysqli->real_connect($this->hostname, $this->username, $this->password)) : ?>
            <script>
                document.location.href = '../error.php';
            </script>
        <?php endif;
//        Select DB
        if (!$mysqli->select_db($this->dbName)) : ?>
            <script>
                document.location.href = '../error.php';
            </script>
        <?php endif;
        return $mysqli;
    }
}