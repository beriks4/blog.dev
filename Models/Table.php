<?php

abstract class Table
{
    protected $table;

    abstract protected static function getTable();

    abstract protected static function init($row);

    public static function getById($id)
    {
        $table = static::getTable();
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT * FROM $table WHERE id = $id;");

        $row = $result->fetch_array();
        $object = static::init($row);

        return $object;
    }

    public static function getAll()
    {
        $table = static::getTable();
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT * FROM $table;");

        $objects = array();
        while ($row = $result->fetch_array()) {
            $objects[] = static::init($row);
        }
        return $objects;
    }

}