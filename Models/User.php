<?php
include_once (__DIR__ . '/../helpers/Connect.php');
include_once ('Post.php');
include_once ('Table.php');
include_once ('Role.php');



class User extends Table
{
    private $id;
    private $name;
    private $email;
    private $password;
    private $rights;

    public function save()
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        if ($this->id){
            $mysqli->query("UPDATE users SET name = \"$this->name\", email = \"$this->email\" WHERE id = $this->id;");
        } else {
            $mysqli->query("INSERT INTO users (name, email, password, role_id) VALUES (\"$this->name\", \"$this->email\", \"$this->password\", \"$this->rights\");");
        }
    }

    public static function delete($id) {
        $connect = new Connect();
        $mysqli = $connect->connect();

        Post::deleteUserPosts($id);
        $mysqli->query("DELETE FROM users WHERE id = $id;");
    }

    public static function check($property_name, $property)
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT * FROM users WHERE $property_name = \"$property\";");
        if (mysqli_num_rows($result) == 0) {
           //not found
            return $check = false;
        } else {
            //found
            return $check = true;
        }
    }

    public static function getByEmail($email)
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT * FROM users WHERE email = \"$email\";");

        $row = $result->fetch_array();
        $user = self::init($row);

        return $user;
    }

    public function getPostsCount()
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT users.id, COUNT(id_u) FROM posts RIGHT OUTER JOIN users ON users.id = posts.id_u WHERE users.id = $this->id GROUP BY id ;");
        $row = $result->fetch_array();
        $count = $row[1];

        return $count;
    }

    public function getUserPosts()
    {
        $posts = Post::getByUserId($this->id);
        return $posts;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }
    public function __get($property)
    {
        return $this->$property;
    }

    public function getRole() {
        return Role::getById($this->rights);
    }

    protected static function init($row)
    {
        $user = new User();
        $user->id = $row[0];
        $user->name = $row[1];
        $user->email = $row[2];
        $user->password = $row[3];
        $user->rights = $row[4];

        return $user;
    }

    protected static function getTable()
    {
       return 'users';
    }
}