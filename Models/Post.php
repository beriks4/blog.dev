<?php
include_once (__DIR__ . '/../helpers/Connect.php');
include_once ('Table.php');

class Post extends Table
{
    private $id;
    private $name;
    private $date;
    private $content;
    private $id_u;

    public function save()
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        if($this->id) {
            $mysqli->query("UPDATE posts SET name = \"$this->name\", content = \"$this->content\", date = NOW() WHERE id = $this->id;");
        } else {
            $mysqli->query("INSERT INTO posts (name, date, content, id_u) VALUES (\"$this->name\", NOW(), \"$this->content\", $this->id_u);");
        }
    }

    public static function delete($id) {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $mysqli->query("DELETE FROM posts WHERE id = $id;");
    }

    public static function deleteUserPosts($id_u) {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $mysqli->query("DELETE FROM posts WHERE id_u = $id_u;");
    }

    public static function getPostsPage($current_page, $on_page = 10) {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $offset = $current_page * $on_page - $on_page;

        $result = $mysqli->query("SELECT * FROM posts ORDER BY date DESC LIMIT $offset, $on_page;");

        $posts = array();
        $i = 0;
        while ($row = $result->fetch_array()) {
            $posts[$i] = self::init($row);
            $i++;
        }
        return $posts;
    }

    public static function getPostsCount() {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT COUNT(*) FROM posts;");
        $row = $result->fetch_array();
        $count = $row[0];

        return $count;
    }

    public static function getByUserId($id)
    {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $result = $mysqli->query("SELECT * FROM posts WHERE id_u = $id;");

        $posts = array();
        $i = 0;
        while ($row = $result->fetch_array()) {
            $posts[$i] = self::init($row);
            $i++;
        }
        return $posts;

    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }
    public function __get($property)
    {
        return $this->$property;
    }

    protected static function init($row)
    {
        $post = new Post();
        $post->id = $row[0];
        $post->name = $row[1];
        $post->date = $row[2];
        $post->content = $row[3];
        $post->id_u = $row[4];

        return $post;
    }
    protected static function getTable()
    {
        return 'posts';
    }
}