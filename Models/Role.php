<?php
include_once('Table.php');
include_once (__DIR__ . '/../helpers/Connect.php');


class Role extends Table
{
    private $id;
    private $description;
    private $level;

    public function save() {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $mysqli->query("INSERT INTO roles (description, access_level) VALUES (\"$this->description\", $this->level);");
    }

    public static function delete($id) {
        $connect = new Connect();
        $mysqli = $connect->connect();

        $mysqli->query("DELETE FROM roles WHERE id = $id;");
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }
    public function __get($property)
    {
        return $this->$property;
    }

    protected static function init($row)
    {
        $role = new Role();
        $role->id = $row[0];
        $role->description = $row[1];
        $role->level = $row[2];

        return $role;
    }
    protected static function getTable()
    {
        return 'roles';
    }
}