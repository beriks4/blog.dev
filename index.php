<?php include_once('Views/navbar.php');
include_once('Models/Post.php');
include_once('Models/User.php');

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$postbase = Post::getPostsPage($page);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <title>Главная</title>
    <link href="/css/create.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row well">
        <div class=" col-lg-8">
            <legend class="text-center">Новости</legend>
            <?php if(Post::getPostsCount() == 0): ?>
                <h2>Здесь пока нет ни одной записи.</h2>
            <?php else:
            foreach ($postbase as $post): ?>
                <h2><?= $post->name; ?></h2>
                <div id="post" class="col-lg-12">
                    <?= nl2br($post->content); ?>
                </div>
                <h6 class="text-right"><?= $post->date; ?> by <?= User::getById($post->id_u)->name; ?></h6>
                <hr/>
            <?php endforeach;
            $pages_count = floor((Post::getPostsCount() - 1) / 10) + 1; ?>
            <div class="text-center">
            <ul class="pagination">
                <?php if($page == 1): ?>
                    <li class="disabled"><a href="#">&laquo;</a></li>
                <?php else: ?>
                <li><a href="index.php?page=<?= $page - 1; ?>">&laquo;</a></li>
                <?php endif;
                for ($i = 1; $i <= $pages_count; $i++):
                    if ($i == $page): ?>
                        <li class="active"><a href="index.php?page=<?= $i; ?>"><?= $i; ?></a></li>
                    <?php else: ?>
                        <li><a href="index.php?page=<?= $i; ?>"><?= $i; ?></a></li>
                    <?php endif;
                endfor;
                if($page == $pages_count): ?>
                    <li class="disabled"><a href="#">&raquo;</a></li>
                <?php else: ?>
                <li><a href="index.php?page=<?= $page + 1; ?>">&raquo;</a></li>
                <?php endif; ?>
            </ul>
            </div>
            <?php endif; ?>
        </div>
        <div class=" col-lg-3 col-lg-offset-1">
            <legend class="text-center">Действия</legend>
            <?php
            if (!$_SESSION) : ?>
                <a href="Views/login.html" class="btn btn-default btn-block">Авторизация</a>
            <?php else : ?>
                <a href="Views/Post/create.php?id=<?= $_SESSION['id'] ?>" class="btn btn-default btn-block">Добавить
                    пост</a>
            <?php endif; ?>
            <a href="Views/User/create.php" class="btn btn-default btn-block">Создание пользователя</a>
            <a href="Views/User/index.php" class="btn btn-default btn-block">Список всех пользователей</a>
            <a href="Views/Role/index.php" class="btn btn-default btn-block">Список ролей</a>
            <a href="Views/Role/create.php" class="btn btn-default btn-block">Создание ролей</a>

        </div>
    </div>
    <div class="row">
        <div class="modal-footer">
            <h6>Boris Petrenko, 2016</h6>
        </div>
    </div>
</div>
</body>
</html>