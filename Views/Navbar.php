<?php
session_start();
$time = time();
if ($_SESSION AND $time - $_SESSION['time'] > 60 * 15) {
    session_unset();
    session_destroy();

} elseif($_SESSION AND $time - $_SESSION['time'] < 60 * 15) {
    $_SESSION['time'] = time();
} ?>
<script src="/js/jquery-1.12.2.js" type="text/javascript"></script>
<script src="/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/css/create.css" type="text/css"></script>
<meta charset="utf-8">
<nav class="navbar navbar-fixed-top navbar-static-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Blog.dev</a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/index.php">Главная</a></li>
                <?php if (!$_SESSION) : ?>
                    <li><a href="/Views/login.html">Login</a></li>
                <?php endif;
                if ($_SESSION) :
                    $name = $_SESSION['name']; ?>
                    <li><a href="/Controllers/logout.php">Logout</a></li>
                    <li><p class="navbar-text">Hello, <?= $name ?> </p></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>