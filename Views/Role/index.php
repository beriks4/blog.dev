<?php include_once('../Navbar.php');
if (!$_SESSION) : ?>
    <script>
        document.location.href = '../../403.html';
    </script>
<?php endif; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Список ролей </title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/create.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row well">
    <?php
    include_once('../../Models/Role.php');

    $rolebase = Role::getAll(); ?>
    <legend>Список ролей:</legend>
<table class="table table-bordered table-striped">
    <tr>
        <th>Id</th>
        <th>Описание</th>
        <th>Уровень доступа</th>
        <th>Действия</th>
    </tr>
    <?php foreach ($rolebase as $role) : ?> <!--тут для тех ролей которые нельзя удалить-->
        <tr>
            <td> <?= $role->id ?></a></td>
            <td> <?= $role->description ?> </td>
            <td> <?= $role->level ?> </td>
            <?php if($role->id >= 1 AND $role->id <= 3) : ?>
            <td>Удаление невозможно</td>
            <?php else : ?>
            <td><a href="../../Controllers/delete.php?obj=role&id=<?= $role->id ?>&access=0" class="btn btn-block btn-info">Удалить</a></td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
</table>
    </div>
</div>
</body>
</html>
