<?php include_once('../Navbar.php');
include_once('../../Models/User.php');
$rights = User::getById($_SESSION['id'])->getRole()->level;
if (!$_SESSION OR ($rights <= 3)): ?>
<script>
    document.location.href = '../../403.html';
</script>
<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Создание ролей</title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery-1.12.2.js"></script>
    <link href="../../css/create.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="well col-md-4 col-md-offset-4" id="center">
            <legend>Создание ролей</legend>
            <form method="post" id="form">
                <fieldset>
                    <div class="control-group">
                        <input type="text" class="form-control" placeholder="Название роли" name="role"
                               size="30">
                    </div>
                    <div id="errrole"></div>
                    <div class="control-group">
                        <input type="text" class="form-control" placeholder="Уровень доступа(1-6)" name="level"
                               size="30">
                    </div>
                    <div id="errlevel"></div>
                    <div id="ok"></div>
                    <input type="submit" name="submit" class="btn btn-block btn-primary" id="submit" value="Создать">
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="../../js/add-role.js"></script>
</body>
</html>