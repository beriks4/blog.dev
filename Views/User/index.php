<?php include_once('../Navbar.php');
if(!$_SESSION) : ?>
    <script>
        document.location.href = '../../403.html';
    </script>
<?php endif; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Список пользователей </title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/create.css" rel="stylesheet">
</head>
<body>
<div class="container">
<div class="row well">
    <?php
    include_once('../../Models/User.php');

    $rights = User::getById($_SESSION['id'])->getRole()->level;
    $userbase = User::getAll(); ?>
    <legend>Список пользователей:</legend>
<table class="table table-bordered table-striped">
    <tr>
        <th>Имя</th>
        <th>E-mail</th>
        <th>Кол-во постов</th>
        <th>Действия</th>
    </tr>

    <?php if ($_SESSION) :
        foreach ($userbase as $user) : ?>
            <tr>
                <?php if($_SESSION['name'] == $user->name ) : ?> <!-- For users -->
                    <td><a href="../Post/index.php?target=<?= $user->id ?>"><span class="glyphicon glyphicon-menu-right"></span> <?= $user->name ?></a></td>
                <?php elseif($rights >= 3) : ?> <!-- For admins -->
                    <td><a href="../Post/index.php?target=<?= $user->id ?>"> <?= $user->name ?></a></td>
                <?php else : ?>
                    <td><a href=""> <?= $user->name ?></a>
                <?php endif; ?>
                <td> <?= $user->email ?> </td>
                <td><div class="btn-group">
                        <a class=" dropdown-toggle" data-toggle="dropdown"> <?= $user->getPostsCount(); ?> </a>
                        <ul class="dropdown-menu">
                            <?php $postbase = $user->getUserPosts();
                            foreach($postbase as $post) : ?>
                                <li><a href="../Post/edit.php?id=<?= $post->id_u; ?>&post_id=<?= $post->id; ?>"><?= $post->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div> </td>
                <?php if($_SESSION['name'] == $user->name) : ?>
                    <td class="col-lg-2"><a href="../../Controllers/delete.php?obj=user&id=<?= $user->id ?>&access=<?= $user->id ?>" class="btn btn-block btn-info">Удалить</a></td>
                <?php elseif($rights == 6) : ?>
                    <td class="col-lg-2"><a href="../../Controllers/delete.php?obj=user&id=<?= $user->id ?>&access=<?= $user->id ?>" class="btn btn-block btn-info">Удалить</a></td>
                <?php endif; ?>
            </tr>
        <?php endforeach;
    else: //этот блок вообще походу нужно удалить
        foreach ($userbase as $user) : ?> <!--тут для лохов которые не залогинились-->
            <tr>
                <td><a href=""> <?= $user->name ?></a></td>
                <td> <?= $user->email ?> </td>
                <td> <div class="btn-group">
                        <a class=" dropdown-toggle" data-toggle="dropdown"> <?= $user->getPostsCount(); ?> </a>
                        <ul class="dropdown-menu">
                            <?php $postbase = $user->getUserPosts();
                            foreach($postbase as $post) : ?>
                                <li><a href="../Post/edit.php?id=<?= $post->id_u; ?>&post_id=<?= $post->id; ?>"><?= $post->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div> </td>
            </tr>
        <?php endforeach;
    endif; ?>
</table>
</div>
</div>
</body>
</html>