<?php include_once('../Navbar.php');
include_once('../../Models/User.php');
include_once('../../Models/Role.php');

if ($_SESSION) $rights = User::getById($_SESSION['id'])->getRole()->level;
if (!$_SESSION OR ($rights <= 3)): ?>
    <script>
        document.location.href = '../../403.html';
    </script>
<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Создание пользователя</title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery-1.12.2.js"></script>
    <link href="../../css/create.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="well col-md-4 col-md-offset-4" id="center">
            <legend>Создание пользователя</legend>
            <form method="post" id="form">
                <fieldset>
                    <div class="control-group">
                        <input type="text" class="form-control" placeholder="Имя пользователя" name="name" id="name"
                               size="30">
                    </div>
                    <div id="errname"></div>
                    <div class="control-group">
                        <input type="text" class="form-control" placeholder="E-mail пользователя" name="email"
                               id="email" size="30">
                    </div>
                    <div id="erremail"></div>
                    <div class="control-group">
                        <input type="password" class="form-control" placeholder="Пароль пользователя" name="pass"
                               id="pass" size="30">
                    </div>
                    <div id="errpass"></div>
                    <div class="control-group">
                        <input type="password" class="form-control" placeholder="Повторите пароль" name="pass1"
                               id="pass1" size="30">
                    </div>
                    <div id="errpass1"></div>
                    <div class="control-group">
                        <select name="rights" class="form-control">
                            <?php $rolebase = Role::getAll();
                            foreach($rolebase as $role): ?>
                            <option value="<?= $role->id ?>"><?= $role->description ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="submit" name="submit" class="btn btn-block btn-primary" id="submit" value="Создать">
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div id="ans" class="invisible"></div>
<script src="../../js/add-user.js"></script>
</body>
</html>