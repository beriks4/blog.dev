<?php include_once('../Navbar.php');
$target = trim($_REQUEST['target']);
include_once('../../Models/User.php');

if ($_SESSION) $rights = User::getById($_SESSION['id'])->getRole()->level;
if (!$_SESSION OR (($_SESSION['id'] != $target) AND $rights <= 2)) : ?>
<script>
    document.location.href = '../../403.html';
</script>
<?php endif; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> Список постов </title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style>
        table {
            table-layout: fixed;
        }
        td {
            overflow: auto;
            height: 50px;
            max-height: 50px;
            word-break: break-all;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row well">
    <?php
    include_once('../../Models/User.php');



    if (strripos($target, '@') === false) {
        $user = User::getById($target);
    }
    else {
        $user = User::getByEmail($target);
    }

    $userposts = $user->getUserPosts();
    ?>

    <legend>Посты пользователя  <?= $user->name ?>:</legend>
    <table class="table table-bordered table-striped">
    <col width="10%">
    <col width="60%">
    <col width="10%">
    <col width="16%">
        <tr>
            <th>Название</th>
            <th>Пост</th>
            <th>Дата</th>
            <th>Действия</th>
        </tr>

        <?php foreach ($userposts as $post) : ?>

            <tr>
                <td> <?= $post->name ?> </td>
                <td><div id="table"> <?= nl2br($post->content) ?> </div></td>
                <td> <?= $post->date ?> </td>
                <?php if ($_SESSION) : ?>
                <td><a href="../../Controllers/delete.php?obj=post&id=<?= $post->id ?>&access=<?= $user->id ?>" class="btn btn-block btn-info">Удалить</a>
                    <a href="../Post/edit.php?id=<?= $post->id_u; ?>&post_id=<?= $post->id; ?>" class="btn btn-block btn-info">Редактировать</a>
                </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
        <?php if ($user->getPostsCount() == 0) : ?>
            <h4>Постов не найдено.</h4><hr/>
        <? endif;?>
        <div class="row col-lg-10">
            <div class="col-lg-3">
                <a href="../User/index.php" class="btn btn-info btn-block">Назад</a>
            </div>
            <div class="col-lg-3">
                <a href="create.php?id=<?= $user->id ?>" class="btn btn-info btn-block">Добавить пост</a>
            </div>
        </div>
    </div>
</div>
</form>
</body>
</html>