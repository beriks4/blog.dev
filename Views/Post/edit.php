<?php include_once('../Navbar.php');
include_once('../../Models/User.php');
include_once('../../Models/Post.php');

$target = trim($_REQUEST['id']);
$post_id = trim($_REQUEST['post_id']);
$post = Post::getById($post_id);

$rights = User::getById($_SESSION['id'])->getRole()->level;
if (!$_SESSION OR ($_SESSION['id'] != $target AND ($rights <= 3))) : ?>
    <script>
        document.location.href = '../../403.html';
    </script>
<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Редактировать пост</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <title>Login</title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery-1.12.2.js"></script>
    <script src="../../js/jquery.validate.min.js" charset="UTF-8"></script>
    <link href="../../css/create.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="well col-md-10 col-md-offset-1" id="center">
            <legend class="text-center">Редактирование поста</legend>
            <form method="post" action="../../Controllers/Post/edit.php" id="form" accept-charset="UTF-8">
                <fieldset>
                    <div class="control-group">
                        <input type="hidden" name="post_id" value="<?= $post_id; ?>" >
                        <input type="text" maxlength="35" class="form-control" placeholder="Название поста" name="name" id="name" size="30" value="<?= $post->name ?>">
                    </div>
                    <div class="control-group">
                        <textarea style="resize: vertical" type="text" maxlength="500" class="form-control" placeholder="Ваш пост..." name="content" id="message" rows="4"><?= $post->content ?></textarea>
                    </div>
                    <button type="submit" name="submit" class="btn btn-block btn-primary">Применить</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="../../js/validate.js"></script>
</body>
</html>