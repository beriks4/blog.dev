<?php include_once('../Navbar.php');
include_once('../../Models/User.php');
$target = trim($_REQUEST['id']);
$rights = User::getById($_SESSION['id'])->getRole()->level;
if (!$_SESSION OR ($_SESSION['id'] != $target AND ($rights <= 3))) : ?>
    <script>
        document.location.href = '/403.html';
    </script>
<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Добавить пост</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <title>Login</title>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery-1.12.2.js"></script>
    <script src="../../js/jquery.validate.min.js" charset="UTF-8"></script>
    <link href="../../css/create.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="well col-md-10 col-md-offset-1" id="center">
            <legend class="text-center">Добавление поста</legend>
            <form method="post" action="../../Controllers/Post/create.php" id="form" accept-charset="UTF-8">
                <fieldset>
                    <div class="control-group">
                        <input type="hidden" name="id" value="<?= trim($_REQUEST['id']) ?>" >
                        <input type="text" maxlength="35" class="form-control" placeholder="Название поста" name="name" id="name" size="30">
                    </div>
                    <div class="control-group">
                        <textarea style="resize: vertical" type="text" maxlength="500" class="form-control" placeholder="Ваш пост..." name="content" id="message" rows="4"></textarea>
                    </div>
                    <button type="submit" name="submit" class="btn btn-block btn-primary">Создать</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="../../js/validate.js"></script>
</body>
</html>